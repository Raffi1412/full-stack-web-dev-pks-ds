<?php

class Hewan
{
    public $nama;
    public $legth;
    public $skill;
    public $darah;

    public function __construct($nama, $legth, $skill, $darah = 50) 
    {
       $this->nama = $nama;
       $this->legth = $legth;
       $this->skill = $skill;
       $this->darah = $darah;
    }

   public function atraksi()
   {
       return $this ->nama . " sedang " . $this->skill . '.';
    }
}

abstract class Fight extends Hewan
{
  public $attack;
  public $defence;

  function serang() 
    {
      return $harimau->name . " sedang menyerang " . $elang->name;
    }
}

class Elang extends Hewan
{
  use Hewan, Fight;
  public function tampilkan(){
    return $this->legth . $this->skill . $this->attack . $this->defence ;
  }
}

  //  $harimau = new Hewan('harimau_1', 4, 'lari cepat');
  //  $elang = new Hewan('elang_3', 2, 'terbang tinggi');
  //  echo $harimau->atraksi();
  $elang = new Hewan('elang', 2, 'terbang tinggi');
  $elang = new Fight(10, 5);
  $harimau = new Fight(7, 8);
  $elang = new Elang();
  echo $elang->tampilkan();
  echo '<br><br>';
  echo $harimau->getInfoHewan('Harimau_2');
  echo '<br><br>';
  $elang->diserang($harimau);
  echo '<br><br>';
  echo $elang->getInfoHewan('Elang_1');